import '../styles/style.css'

import { Button, Modal } from 'antd';
    export const BtnHeader = () => {
    const [modal, contextHolder] = Modal.useModal();
    const countDown = () => {
        let secondsToGo = 5;
        const instance = modal.success({
        title: 'Поздравляем! Вы получаете скидку 15%',
        content: `Используйте промокод ЛЕТО23 и получите скидку на первый заказ`,
        });
        const timer = setInterval(() => {
        secondsToGo -= 1;
        instance.update({
            content: `Успей воспользоваться. Осталось ${secondsToGo} секунд`,
        });
        }, 10000);
        setTimeout(() => {
        clearInterval(timer);
        instance.destroy();
        }, secondsToGo * 1000);
    };
    return (
        <>
        <Button style={{color:'#fff'}} onClick={countDown}>Получить подарок</Button>
        {contextHolder}
        </>
    );
    };
