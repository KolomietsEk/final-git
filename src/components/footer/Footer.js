import "./style.css"
import vk from "./../../img/icon/vk.svg"
import twitter from "./../../img/icon/twitter.svg"
import link from "./../../img/icon/linkedIn.svg"
import inst from "./../../img/icon/instagram.svg"

const Footer = () => {
    return (<footer className="footer">
    <div className="container">
        <div className="footer__wrapper">
        <ul className="social">
            <li className="social__item">
                <a href="#"><img src ={vk} /></a>
            </li>
            <li className="social__item">
                <a href="#"><img src ={twitter} /></a>
            </li>
            <li className="social__item">
                <a href="#"><img src ={link} /></a>
            </li>
            <li className="social__item">
                <a href="#"><img src ={inst} /></a>
            </li>
        </ul>
        <div className="copyright">
            <p>© 2023 GRAPHIC-DESIGN.com</p>
        </div>
        </div>
    </div>
  </footer>)
}

export default Footer