import "./style.css"
import gitHubIcon from "./../../img/icon/gitHub-black.svg"
import { Button } from "antd"

import { useNavigate } from "react-router-dom"



const BtnGitHub = ({link}) => {
    const navigate = useNavigate()
    const handleClick = () => {
        navigate('/contacts')
    }
    return (
        // <a href={link} className="btn-outline">
        //             <img src={gitHubIcon} alt="" />
        //             GitHub repo
        // </a>
        <Button onClick = {handleClick} type="primary">Оставить заявку</Button>
    )
}

export default BtnGitHub