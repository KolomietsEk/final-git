import "./style.css"

import {BtnHeader} from "../BtnHeader"

const Header = () => {
    return (<header className="header">
    <div className="header_wrapper">
        <h1 className="header_title">Создаем дизайн<br /> 
                                 для вашего бизнеса
        </h1>
        <div className="header_text">
            <p>разработка фирменного стиля, брендирование, 3D-графика, упаковка</p>
        </div>
        <BtnHeader />
    </div>
</header>)
}

export default Header