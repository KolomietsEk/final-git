import BtnGitHub from "../components/btnGitHub/BtnGitHub"
import img from "./../img/projects/01-big.jpg"
import { useParams } from "react-router-dom"
import {projects} from "./../helpers/projectsList"


const Project = () => {
    const {id} = useParams()
    const project = projects[id]
    return (
        <main className="section">
        <div className="container">
            <div className="project-detalis">
                <h1 className="title-1">{project.title}</h1>
                <img src={project.imgBig} alt="" className="project-detalis__cover" />
                <div className="project-detalis__desc">
                    <p>Skills: {project.skills}</p>
                </div>
                
                <BtnGitHub link="http://github.com"/>

            </div>
        </div>
    </main>
    )

}

export default Project