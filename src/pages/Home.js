import Project from '../components/project/Project'
import Header from './../components/header/Header'

import { projects } from "./../helpers/projectsList"


const Home = () => {
    return (
    <>
    <Header />
    <main className="section">
      <div className="container">
          <h2 className="title-1">Проекты</h2>
          <ul className="projects">
            {projects.map((project, index) => {
                return <Project key={index} title={project.title} img={project.img} index={index}/>
            })}
             
              {/*<li className="project">
                  <a href="./packag.html">
                      <img src={two} alt="project img" className="project__img" />
                      <h3 className="project__title">Дизайн упаковки</h3>
                  </a>
              </li>
              <li className="project">
                  <a href="./magazine.html">
                      <img src={three} alt="project img" className="project__img" />
                      <h3 className="project__title">Верстка журналов</h3>
                  </a>
              </li>
              <li className="project">
                  <a href="./business-card.html">
                      <img src={four} alt="project img" className="project__img" />
                      <h3 className="project__title">Визитки</h3>
                  </a>
              </li>
              <li className="project">
                  <a href="./poster.html">
                      <img src={five} alt="project img" className="project__img" />
                      <h3 className="project__title">Постеры</h3>
                  </a>
              </li>
              <li className="project">
                  <a href="./presentation.html">
                      <img src={six} alt="project img" className="project__img" />
                      <h3 className="project__title">Презентации</h3>
                  </a>
              </li>*/}
          </ul>
      </div>
    </main>
    </>)
}

export default Home