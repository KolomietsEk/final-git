import { Form, Input, Button, Select } from "antd"
import { useNavigate } from "react-router-dom"
import '../../styles/style.css'


const { Option } = Select

const Contacts = () => {
    const navigate = useNavigate()

    const clickToGo = () => {
        navigate ('./result')
    }

    const prefixSel = (
        <Form.Item name="prefix" noStyle>
            <Select style={{width: 110}}>
                <Option value="+37529">+37529</Option>
                <Option value="+37544">+37544</Option>
                <Option value="+37533">+37533</Option>
            </Select>
        </Form.Item>
    )

    return (
    <main className="section">
    <div className="container">
        <h1 className="title-1">Контакты</h1>
        <Form>
            <Form.Item
                label="Имя"
                name="username"
                rules={[
                    {
                    required: true,
                    message: 'Введите свое имя',
                    },
                ]}
                >
            <Input />
            </Form.Item>

            <Form.Item
            label="Фамилия"
            name="secondname"
            rules={[
                {
                required: true,
                message: 'Введите свою фамилию',
                },
            ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
            label="Телефон"
            name="phone"
            rules={[
                {
                    required: true,
                    message: 'Введите номер телефона',
                },
            ]}
            >
                <Input addonBefore={prefixSel}/>
            </Form.Item>

            <Form.Item>
            <Button onClick={clickToGo} type="primary" htmlType="submit">
                Отправить
            </Button>
            </Form.Item>
  </Form>
    {/* <ul className="content-list">
        <li className="content-list__item">
            <h2 className="title-2">Location</h2>
            <p>Minsk, Belarus</p>
        </li>
        <li className="content-list__item">
            <h2 className="title-2">Telegram / Viber</h2>
            <p><a href="tel:+375291234567">+375 (29) 123-45-67</a></p>
        </li>
        <li className="content-list__item">
            <h2 className="title-2">Email</h2>
            <p><a href="mailto:graphic@gmail.com">graphic@gmail.com</a></p>
        </li>
    </ul>      */}
    </div>
</main>)
}

export default Contacts