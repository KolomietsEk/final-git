import { Route, Routes } from "react-router-dom"
import Contacts from "./Contacts"
import { ResultContacts } from "./ResultContacts"

export const IndexContacts = () => {
    return (
        <Routes>
            <Route index element={<Contacts />} />
            <Route path='/result' element={<ResultContacts />} />
        </Routes>
    )
}