import { SmileOutlined } from '@ant-design/icons';
import { Result, Button } from 'antd';


export const ResultContacts = () => {
    return (
        <div>
        <Result
                icon={<SmileOutlined />}
                title="Спасибо, что выбрали GRAPHIC DESIGN!"
                subTitle="Наш специалист с Вами свяжется"
        />
        </div>
    )
}