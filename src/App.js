import "./styles/style.css"

import { BrowserRouter as Router, Routes, Route } from "react-router-dom"

import Navbar from "./components/navbar/Navbar"
import Footer from "./components/footer/Footer"
import Home from "./pages/Home"
import Project from "./pages/Project"
import Skills from "./pages/Skills"

import ScrollToTop from "./utils/scrollToTop"
import { IndexContacts } from "./pages/contacts/IndexContacts"




function App() {
  return (
    <div className="App">
      <Router>
        <ScrollToTop />
        <Navbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/projects" element={<Home />} />
            <Route path="/project/:id" element={<Project />} />
            <Route path="/contacts/*" element={<IndexContacts />} />
            <Route path="/skills" element={<Skills />} />
          </Routes>
        <Footer />
      </Router>
      

    </div>
  );
}

export default App;
