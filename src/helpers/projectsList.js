import one from "./../img/projects/01.jpg"
import oneBig from "./../img/projects/01-big.jpg"
import two from "./../img/projects/02.jpg"
import twoBig from "./../img/projects/02-big.jpg"
import three from "./../img/projects/03.jpg"
import threeBig from "./../img/projects/03-big.jpg"
import four from "./../img/projects/04.jpg"
import fourBig from "./../img/projects/04-big.jpg"
import five from "./../img/projects/05.jpg"
import fiveBig from "./../img/projects/05-big.jpg"
import six from "./../img/projects/06.jpg"
import sixBig from "./../img/projects/06-big.jpg"


const projects = [
    {
        title:'Логотипы',
        skills: 'Adobe Illustrator, Turbologo, Canva, Coreldraw, DesignEvo',
        img: one,
        imgBig:oneBig,
        link: 'https://ant.design/components/button',
    },
    {
        title:'Дизайн упаковки',
        skills: 'Adobe Illustrator, InDesign, Photoshop, Acrobat Professional, Dreamweaver, QuarkXPress, Foto 3D CX 2, Enfold 3D',
        img: two,
        imgBig: twoBig,
        link: 'https://ant.design/components/button',
    },
    {
        title:'Верстка журналов',
        skills: 'Quark Xpress, Adobe InDesign, PageMaker, XPress',
        img: three,
        imgBig: threeBig,
        link: 'https://ant.design/components/button',
    },
    {
        title:'Визитки',
        skills: 'CorelDRAW, FreeHand, Adobe InDesign, Adobe Illustrator, Photoshop',
        img: four,
        imgBig: fourBig,
        link: 'https://ant.design/components/button',
    },
    {
        title:'Постеры',
        skills: 'Canva, Piktochart, Adobe Spark, CorelDRAW, MyCreativeShop, Desygner',
        img: five,
        imgBig: fiveBig,
        link: 'https://ant.design/components/button',
    },
    {
        title:'Презентации',
        skills: 'SmartDraw, VideoScribe, SlideDog, LibreOffice, ProShow Producer',
        img: six,
        imgBig: sixBig,
        link: 'https://ant.design/components/button',
    }

]

export {projects}